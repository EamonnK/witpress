package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Blob;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.persistence.OneToMany;

import controllers.Accounts;
import play.db.jpa.Model;

@Entity
public class Comment extends Model{
	//fields that make comment class
	public String content;
	public Date date;
	//foreign key connecting post to comment
	@ManyToOne
	public Post post;
	//foreign key connecting user to comment
	@ManyToOne
	public User from;
	
	/**
	 * Contructor for objects of comment class.
	 * 
	 * @param String content
	 * @param String post
	 */
	public Comment(String content, Post post){
		this.content = content;
		this.post    = post;
		from         = Accounts.getCurrentUser();
		date         = new Date();
		
		
	}
	
}