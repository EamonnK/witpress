package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import play.db.jpa.Blob;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import javax.persistence.OneToMany;

import controllers.Accounts;
import play.db.jpa.Model;

@Entity
public class User extends Model
{ //fields that make object of user class
  public String email;
  public String firstName;
  public String lastName;
  public String password;
  
  /**
   * Constructor for objects of User class
   * 
   * @param String email
   * @param String firstName
   * @param String lastName
   * @param String password
   */
  public User(String email, String firstName, String lastName, String password){
	this.email       = email;
	this.firstName   = firstName;
    this.lastName    = lastName;
    this.password    = password;
  }
  
  /**
   * Method to find user by email.
   * 
   * @param String email
   * @return User
   */
  public static User findByEmail(String email){
    return find("email", email).first();
  }

  /**
   * Method to check input password against stored value.
   * 
   * @param String password
   * @return boolean
   */
  public boolean checkPassword(String password){
    return this.password.equals(password);
  }
}