package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Blob;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.persistence.OneToMany;

import controllers.Accounts;
import play.db.jpa.Model;

@Entity
public class Post extends Model{
	//fields that make object of post class.
	public String title;
	public String content;
	public Date date;
	//foreign key linking post to user writes it
	@ManyToOne
	public User from;
	
	/**
	 * Constructor for objects of post class.
	 * 
	 * @param String title
	 * @param String content
	 */
	public Post(String title, String content){
		this.title   = title;
		this.content = content;
		from         = Accounts.getCurrentUser();
		date         = new Date();
	}
}