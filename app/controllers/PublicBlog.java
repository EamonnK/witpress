package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class PublicBlog extends Controller{
	
	/**
	 * Method to render publicblog page, rendering all users to page
	 * along with a hashmap of user keys and arraylists of posts made
	 * by each user.
	 */
	public static void publicblog(){
		
		List<User> users = User.findAll();
		List<Post> allPosts = Post.findAll();
		HashMap<User, ArrayList<Post>> publicBlogs = new HashMap<>(); 
		
		for(User u : users){
			ArrayList<Post> myPosts = new ArrayList<>();
			for(Post p : allPosts){
				if(u == p.from){
					myPosts.add(p);	
				}
				if(myPosts.size() > 0){
				publicBlogs.put(u, myPosts);
				}
			}
		}
		render(users, publicBlogs);
	}
}