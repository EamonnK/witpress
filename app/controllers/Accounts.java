package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller {

	/**
	 * Method to render signup html.
	 */
    public static void signup() {
        render();
    }
    
    /**
     * Method to render login html.
     */
    public static void login(){
    	render();
    }
    
    /**
     * Method to logout user by clearing session map
     * rendering login page.
     */
    public static void logout(){
    	session.clear();
    	login();
    }
    
    /**
     * Method to create a user object with input details
     * and save user to database.
     * 
     * @param email
     * @param firstName
     * @param lastName
     * @param password
     */
    public static void register(String email, String firstName, String lastName, String password){
    	Logger.info(email + " " + firstName + " " + lastName + " "  +  " " + password);
	    User user = new User(email, firstName, lastName, password);
	    user.save();
	    login();
    }
    
    /**
     * Method to find user by input email and if successful
     * check input password against user password in database.
     * 
     * @param email
     * @param password
     */
    public static void authenticate(String email, String password) {
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);

	    User user = User.findByEmail(email);
	    if ((user != null) && (user.checkPassword(password) == true)){
	      Logger.info("Authentication successful");
	      session.put("logged_in_userid", user.id);
	      BlogPost.blogpost();
	    } else{
	      Logger.info("Authentication failed");
	      Accounts.login();  
	    }
	 }
    
    /**
     * Method to get logged in user from session map
     * and return user if not null.
     * 
     * @return User
     */
    public static User getCurrentUser(){
		 
		 String userId = session.get("logged_in_userid");
		 if(userId == null){
			 return null;
		 }
		 User logged_in_user = User.findById(Long.parseLong(userId));
		 Logger.info("Logged in user is "+ logged_in_user.firstName);
		 return logged_in_user;
	 }

}