package controllers;


import java.util.Comparator;

import models.Comment;

public class SortByDate implements Comparator<Comment>{

	/**
	 * Comparator to sort objects of comment model by date.
	 */
	@Override
	public int compare(Comment a, Comment b) {
		return b.date.compareTo(a.date);
	}
	
}