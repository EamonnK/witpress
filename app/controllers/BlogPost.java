package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class BlogPost extends Controller{
	
	/**
	 * Method to render blogpost html, finding current user
	 * iterating through all posts and adding posts made by 
	 * user to arraylist, rendering to page.
	 */
	public static void blogpost(){
		User user = Accounts.getCurrentUser();
		if(user == null){
			Accounts.login();
		}else{
	        List<Post> post = Post.findAll();
	        List<Post> yourPosts = new ArrayList<>();
	        for(Post p : post){
	        	if(p.from == user){
	        		yourPosts.add(p);
	        	}
	        }
	        render(yourPosts);
		}
	}
	
	/**
	 * Method to create new post with input values
	 * and save to data base.
	 * 
	 * @param String title
	 * @param String content
	 */
	public static void postBlog(String title, String content){
		Post post = new Post(title, content);
		post.save();
		blogpost();
	}
	
	/**
	 * Method to render particular post to page by post id
	 * iterating through all comments and adding those linked to
	 * post to an arraylist and rendering to page along with user.
	 * 
	 * @param Long postid
	 */
	public static void blog(Long postid){
		User user = Accounts.getCurrentUser();
		Post post = Post.findById(postid);
		List<Comment> comment = Comment.findAll();
		List<Comment> postComments = new ArrayList<>();
		for(Comment c : comment){
			if(c.post == post){
				postComments.add(c);
			}
		}
		Collections.sort(postComments, new SortByDate());
		render(user,post, postComments);
		
		
	}
	
	/**
	 * Method to delete a post from database by input postid, 
	 * from html, removing all comments linked to that post.
	 * 
	 * @param Long postid
	 */
	public static void deletePost(Long postid){
		Post post = Post.findById(postid);
		List<Comment> comment = Comment.findAll();
		for(Comment c : comment){
			if(c.post == post){
				c.delete();
			}
		}
		post.delete();
		blogpost();	
	}
	
	/**
	 * Method to create a comment object with input details
	 * linking comment to post in constructor.
	 * 
	 * @param String content
	 * @param Long postid
	 */
	public static void postComment(String content, Long postid){
		Post post = Post.findById(postid);
		Comment comment = new Comment(content, post);
		comment.save();
		blog(postid);
	}
	
	/**
	 * Method to delete comment from database, finding comment by
	 * input id and re rendering blog page with post to which the commment 
	 * was attached.
	 * 
	 * @param comid
	 * @param postid
	 */
	public static void deleteComment(Long comid, Long postid){
		Comment comment = Comment.findById(comid);
		comment.delete();
		blog(postid);
	}
	
}