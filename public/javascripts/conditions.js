function ischecked(){
	const checked = document.getElementById('conditions').checked;
	if ( checked === false ){
		
		alert("You must agree to terms and conditions to register!");
		return false;
	}
};



//Form validation
$('.ui.form')
.form({
    firstName: {
    identifier: 'firstName',
    rules: [
      {
        type: 'empty',
        prompt: 'Please enter your first name.'
      }
    ]
  },
  
    lastName: {
	identifier: 'lastName',
	rules: [
	  {
	    type: 'empty',
	    prompt: 'Please enter your last name.'
	  }
	]
  },
  
    email: {
    identifier: 'email',
    rules: [
	  {
		type: 'empty',
		prompt: 'Please enter your email.'
	  }
    ]
  },
  
    password: {
    identifier: 'password',
    rules: [
		  {
		    type: 'empty',
		    prompt: 'Please enter a password.'
		  }
		]
	  },
 
});

